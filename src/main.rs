// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::anyhow;
use chrono::prelude::*;
use mysql_async::Pool;
use rocket::fairing::AdHoc;
use rocket::fs::FileServer;
use rocket::http::Status;
use rocket::response::stream::{Event, EventStream};
use rocket::response::Redirect;
use rocket::{Request, State};
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::ops::Deref;
use std::time::Duration;
use tokio::sync::{mpsc, watch};
use tokio::time::interval;

mod query;

type ConsoleWatcher = watch::Receiver<ConsoleState>;
type ConsoleSender = mpsc::Sender<ConsoleState>;

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(status = 500, content_type = "text/html")]
struct Error(Template);

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Self(Template::render(
            "error",
            ErrorTemplate {
                code: 500,
                reason: "Internal Server Error",
                error: Some(value.to_string()),
            },
        ))
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Serialize)]
struct IndexTemplate {}

#[derive(Serialize)]
struct PendingTemplate {}

#[derive(Serialize)]
struct ResultsTemplate {
    results: QueryResults,
}

#[get("/")]
fn index(watcher: &State<ConsoleWatcher>) -> Result<Template> {
    let state = watcher.borrow().clone();
    let template = match state {
        ConsoleState::Nothing => Template::render("index", IndexTemplate {}),
        ConsoleState::Pending { .. } => {
            Template::render("pending", PendingTemplate {})
        }
        ConsoleState::Error { error } => Err(anyhow!(error))?,
        ConsoleState::Results(results) => {
            Template::render("results", ResultsTemplate { results })
        }
    };
    Ok(template)
}

#[derive(Serialize, Clone)]
#[serde(tag = "state")]
enum ConsoleState {
    Nothing,
    Pending { age: DateTime<Utc> },
    Error { error: String },
    Results(QueryResults),
}

#[derive(Serialize, Clone, Debug)]
struct QueryResults {
    age: String,
    results: Vec<ResultRow>,
}

#[derive(Serialize, Clone, Debug)]
struct ResultRow {
    title: String,
    length: usize,
}

#[post("/go")]
async fn go(
    watcher: &State<ConsoleWatcher>,
    sender: &State<ConsoleSender>,
    pool: &State<Pool>,
) -> Redirect {
    let val = watcher.borrow().clone();
    if matches!(val, ConsoleState::Pending { .. }) {
        // Query is already running
        println!("query is already running");
        return Redirect::to("/");
    }
    // Start a new query
    let _ = sender.send(ConsoleState::Pending { age: Utc::now() }).await;
    trigger_query(sender, pool);
    Redirect::to("/")
}

fn trigger_query(sender: &ConsoleSender, pool: &Pool) {
    let sender = sender.clone();
    let pool = pool.clone();
    println!("triggering query...");
    tokio::spawn(async move {
        println!("query spawned...");
        let state = match query::lookup(&pool).await {
            Ok(results) => ConsoleState::Results(QueryResults {
                age: Utc::now().to_string(),
                results,
            }),
            Err(err) => ConsoleState::Error {
                error: err.to_string(),
            },
        };
        let _ = sender.send(state).await;
    });
}

#[get("/events")]
async fn events(rx: &State<ConsoleWatcher>) -> EventStream![] {
    let mut rx = rx.deref().clone();
    EventStream! {
        loop {
            let val = rx.borrow_and_update().clone();
            yield Event::json(&val);
            if rx.changed().await.is_err() {
                break;
            }
        }
    }
}

#[catch(default)]
fn default_catcher(status: Status, _req: &Request) -> Template {
    Template::render(
        "error",
        ErrorTemplate {
            code: status.code,
            reason: status.reason_lossy(),
            error: None,
        },
    )
}

#[launch]
fn rocket() -> _ {
    // watch channel that threads can observe to see the current state
    let (w_sender, w_receiver) = watch::channel(ConsoleState::Nothing);
    // mpsc channel that receives new state events and passes it into the watch channel
    let (m_sender, mut m_receiver) = mpsc::channel::<ConsoleState>(100);
    rocket::build()
        .manage(w_receiver)
        .manage(m_sender)
        .manage(Pool::new(
            toolforge::connection_info!("enwiki")
                .expect("failed to get db connection info")
                .to_string()
                .as_str(),
        ))
        .attach(Template::fairing())
        .attach(Healthz::fairing())
        .mount("/", routes![index, events, go])
        .register("/", catchers![default_catcher])
        .mount("/static", FileServer::from("static"))
        // pass state update events from the mpsc channel into the watch channel
        .attach(AdHoc::on_liftoff("passthrough", |_rocket| {
            Box::pin(async move {
                tokio::spawn(async move {
                    while let Some(state) = m_receiver.recv().await {
                        w_sender.send_replace(state);
                    }
                });
            })
        }))
        // every 60s, check if the state has been pending for more than 15 minutes, if so, change it to an error state
        .attach(AdHoc::on_liftoff("indefinite pending", |rocket| {
            Box::pin(async move {
                let sender = rocket.state::<ConsoleSender>().unwrap().clone();
                let rx = rocket.state::<ConsoleWatcher>().unwrap().clone();
                let mut interval = interval(Duration::from_secs(60));
                tokio::spawn(async move {
                    loop {
                        let val = rx.borrow().clone();
                        if let ConsoleState::Pending { age } = val {
                            let duration = Utc::now() - age;
                            if duration.num_minutes() >= 15 {
                                println!("timed out");
                                let _ = sender.send(ConsoleState::Error { error: "Unknown timeout error. Please try again".to_string() }).await;
                            }
                        }
                        interval.tick().await;
                    }
                });
            })
        }))
}
