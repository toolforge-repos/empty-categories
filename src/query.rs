// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::ResultRow;
use mysql_async::prelude::Queryable;
use mysql_async::Pool;

const MYSQL_QUERY: &str = r#"
/* emptycats.rs SLOW_OK */
SELECT
  page_title,
  page_len
FROM
  categorylinks
  RIGHT JOIN page ON cl_to = page_title
WHERE
  page_namespace = 14
  AND page_is_redirect = 0
  AND cl_to IS NULL
  AND NOT(
    CONVERT(page_title USING utf8) REGEXP '(-importance|-class|assess|_articles_missing_|_articles_in_need_of_|_articles_undergoing_|_articles_to_be_|_articles_not_yet_|_articles_with_|_articles_without_|_articles_needing_|Wikipedia_featured_topics)'
  )
  AND NOT EXISTS (
    SELECT
      1
    FROM
      categorylinks
    WHERE
      cl_from = page_id
      AND (
        cl_to = 'Wikipedia_soft_redirected_categories'
        OR cl_to = 'Disambiguation_categories'
        OR cl_to = 'Monthly_clean-up_category_counter'
        OR cl_to LIKE 'Empty_categories%'
      )
  )
  AND NOT EXISTS (
    SELECT
      1
    FROM
      templatelinks
    JOIN linktarget ON tl_target_id = lt_id
    WHERE
      tl_from = page_id
      AND lt_namespace = 10
      AND (
        lt_title = 'Empty_category'
        OR lt_title = 'Possibly_empty_category'
        OR lt_title = 'Monthly_clean-up_category'
        OR lt_title = 'Maintenance_category_autotag'
      )
  );
"#;

pub(crate) async fn lookup(pool: &Pool) -> anyhow::Result<Vec<ResultRow>> {
    println!("getting connection...");
    let mut conn = pool.get_conn().await?;
    println!("starting query...");
    let rows = conn
        .query_map(MYSQL_QUERY, |(title, length): (String, usize)| ResultRow {
            title: title.replace('_', " "),
            length,
        })
        .await?;
    println!("done!");
    Ok(rows)
}
