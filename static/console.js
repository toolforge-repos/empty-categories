console.log(gstate);
const evtSource = new EventSource("/events");
evtSource.onmessage = (event) => {
	const data = JSON.parse(event.data);
	console.log(data);
	if (gstate != "" && gstate != "Error" && data.state != gstate) {
	    // If our state is outdated, refresh the page
	    window.location = "/";
	}
};
